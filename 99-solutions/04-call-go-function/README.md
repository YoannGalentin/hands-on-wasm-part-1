# 04-call-go-function

## Requirements

- Create a `go.mod` file
- Create a `main.go` file

## go.mod

```text
module 04-call-go-function

go 1.17
```

## main.go

```golang
package main

import (
	"encoding/json"
	"log"
	"syscall/js"
)

type Human struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

func GiveMeHumanJsonObject(this js.Value, args []js.Value) interface{} {

	jsonHuman, err := json.Marshal(
		Human{
			args[0].String(),
			args[1].String()})

	if err != nil {
		log.Fatalf(
			"Error occured during marshaling: %s",
			err.Error())
	}

	JSON := js.Global().Get("JSON")
	jsonString := string(jsonHuman)

	return JSON.Call("parse", jsonString)

}

func main() {

	js.Global().Set(
		"GiveMeHumanJsonObject",
		js.FuncOf(GiveMeHumanJsonObject))

	<-make(chan bool)
}
```

## Build, run, ...

```bash
# build the wasm fil
GOOS=js GOARCH=wasm go build -o main.wasm
```

```bash
# run the http server
node index.js
```

- Open the webpage in your browser
- Open the console (developer tools)

## Add a Go "GiveMeAnotherHumanJsonObject" function

This new function is similar to `GiveMeHumanJsonObject` but I want to call it like that:

```javascript
let bill = GiveMeAnotherHumanJsonObject({
  firstName: "Bill",
  lastName: "Ballantine"
})
```

To get the first argument of the function, you will use this:

```golang
human := args[0]
```

**The type of `human` is `js.Value`**, it's a kind of map, so you can get the value of a field with the `Get` method of `human`.

**Remember**:

* The **`Hello`** function takes two parameters and returns an **`interface{}`** type. 
* The function will be called synchronously from Javascript. 
* The **first** parameter (**`this`**) refers to **JavaScript's global object**. 
* The **second** parameter is a slice of **`[]js.Value`** representing the arguments passed to the Javascript function call.

**See**: https://pkg.go.dev/syscall/js



