package main

import (
	"syscall/js"
)

func main() {

	message := "👋 Hello World 🌍 from Go"

	document := js.Global().Get("document")

	h2 := document.Call("createElement", "h2")
	h2.Set("innerHTML", message)

	h2.Get("style").Set("color","orange")

	document.Get("body").Call("appendChild", h2)

	document.Call("querySelector", "h1").Get("style").Set("color","green")

}
